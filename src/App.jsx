import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Home, Service, Car, Fetch, Login} from './pages';
import Protected from './components/Protected';
import NavBar from './components/NavigationBar';
import Footer from './components/Footer';
import './App.css';

function App() {
  return (
    <div>
      <BrowserRouter>
        <NavBar />
        <Routes>
          <Route exact path="/" element={<Home />}>
          </Route>
          <Route exact path="/login" element={<Login />}>
          </Route>
          <Route path="/service" element={
            <Protected>
              <Service />
            </Protected>
          }>
          </Route>
          <Route path="/car" element={<Car />}>
          </Route>
          <Route path="/cars" element={<Fetch />}>
          </Route>
        </Routes>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
