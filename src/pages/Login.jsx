import { Form, Button } from 'react-bootstrap';
import { useEffect, useState } from 'react';

function Login() {
    const [email, setEmail] = useState('');
    const [isLoggedIn, setisLoggedIn] = useState(false);
    const token = localStorage.getItem('token');

    useEffect(() => {
        token ? setisLoggedIn(true) : setisLoggedIn(false)
    }, [token])

    // useEffect (() => {
    //     console.log('coba');
    // }, [isLoggedIn])

    function handleSubmit(e) {
        // e.preventDefault();
        fetch('http://localhost:3001/api/v1/auth/login', {
            method: 'POST',
            body: { email }
        }).then((response) => {
            return response.json()
        }).then((result) => {
            localStorage.setItem('token', result.token);
        })
    }

    function handleLogout(e) {
        setisLoggedIn(false);
        localStorage.removeItem('token')
    }

    return (
        <div>
            {!isLoggedIn ? (
                <div className='container container-fluid my-5'>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" onChange={(e) => {
                                setEmail(e.target.value)
                            }} value={email} />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                </div>
            ) : (
                <div className='container container-fluid my-5'>
                    <h2>Logout for sure???</h2><br /><br />
                    <Button variant="primary" onClick={handleLogout}>
                        Logout
                    </Button>
                </div>
            )}
        </div>
    )
}

export default Login